## Goals
- Reliable
- Intuitive
- Flexible

## Guide (advise)
- Critical features like automatic locks/unlocks/opening of doors: use 2 or more different conditions or 1 trigger and 1 condition. The goal here is to avoid false unlocks/opens.
- Backup your progress - things break, mistakes happen. Make sure you can roll back changes or move to new hardware easily.
- Plan on stuff breaking - Internet out? Power out? Does the Internet go out with the power even plugged into a UPS? Plan for these changes so you at least know what to expect.
- Make secure (Hide passwords, even when they are weak)
- Keep it simple - There is a fine line here as automations get more complicated. What you can do is make sure it is orginized and try to think of ways to simplify a complicated solution.
- Less notifications make them more useful - Too many and they get ignored.
- Modularize as needed - Zigbee is a pain sometimes with HA, it is easier to let Hue/Osram manage those devices so I can focus on building better automations.
- Too much interaction means something may need to be changed (voice control of lights, manual brightness adjustments, lights going out while in the bathroom)
- Complacency is not good - don't let people get used to stuff not working well. People will secretly be annoyed with the system you developed and you will not be notified when there is a truly concerning issue.
- Communicate with and train your family on new features - This will help keep everyone calm and prepared when something doesn't work or changes. It provides a sense of control for everyone and prepares them. This could be educational for you too as it gives others an opertunity to voice concerns.
- Prepare for a technical support role. Make sure something is reasonably reliable before deploying it. Test things ahead of time. Ditch things or quarantine them if there are problems. Failure to do this will annoy your family.

## Features and settings
- Presence status: "Home", "Away", "Just arrived", "Just left", and "Extended away" triggers
- Thermostat away mode automatically set
- Night mode and morning temperature changes
- Lights out when away
- Motion to trigger lights in main rooms
    - As needed based on ambient lighting (lux) levels
    - "Night" mode sets dimmer "night-light" setting for all motion triggered lights
- Night mode settings (temperature, switch lights to night-light settings)
- Open/Close garage door when leaving/arriving
- Flux: Change temperature of lights throughout the day
- Circle: Limit and track Internet usage
- Circle/IFTTT: Pause Internet voice command
- PiHole: Adblocker, DHCP, and DNS proxy
- Nest auto-schedule with two stage HVAC
- Battery backup with NUTd monitoring
- Lighting and power failure:
    - Osram: Auto shut off of lights after power outage
    - Bedroom lights revert to last state after power outage (hue and Hank bulbs)
- Non-bedroom lights will power on when using physical switch
- Dim/brighten lights when TV is paused via ADB (Mi Box, nVidia Shield)
- When window is open disable HVAC, when all windows are closed HVAC restored to previous state
- Notifications:
    - Leak detection (mentions specific area)
    - Laundry (washer) is done
    - When there is rain and a window is open (mentions specific windows)
    - Quiet alert (light flashing) when switching to night mode and doors are open
    - When leaving work and arriving at home
    - ~~Welcome home message~~ Once novelty wears off and it repeats 10 times in 10 minutes it is not so fun

## Todo
- Find a better battery alert package (battery_level does not map to freindly names)
- Setup ambient color routines for all rooms
    - turn on <color> (blue, red, green, yellow, rotate color) - auto disable flux/auto turn on flux when done
    - activity would be similar to: turn off flux, rotate color script, when turned off turn flux back on (or previous state of flux)
    - Ambient routines: rotate color, candlight, fire, northern
- Trigger Nest fan when temp is 2-5 degrees different during the day and presense is home
- Connect humidifier to thermostat
- Consider using Asus router for IP/MAC phone presence features
- Prevent lights from going out while someone is taking a bath
- Turn on house fan for 15 minutes when window is open
- Detect when to go into night mode without manual trigger
- Notification when high/~~low~~ temperature threshold reached (high)
- Notification when motion, door, window triggered and presence is away (security)
- Notification to home person when wash/dryer is done (low)
- Notification to home person when dishwasher is done (low)
- Notification to home person when coffee is done (low)
- Improve outdoor holiday lighting
- Install outdoor lighting
- Setup long term graphes
- Consider moving away from Life360 to be a more contained/isolated system
    - combination of OwnTracks and dhcp presence
- Setup sleep and WOL for VMware infrastructure (lower power consumption)
- ~~Setup Tuya with Smart Life devices and get Tuya on HA~~
- ~~Dim/brighten lights when TV is paused via ADB (Mi Box, nVidia Shield) (test - may not like this feature)~~
- ~~Consider PiHole/DHCP standalone setup with RasPi, possibly moving Wifi AP as router/gw~~
- ~~Move PiHole/DHCP/DNS off NUC/Docker/Hassio~~
- ~~Move thermostat~~
- ~~Fix/diagnose server~~ Seems to have been a combination of issues with ssl cert expired and DNS.
- ~~Allow some lights to turn on with dumb switch~~
- ~~Move Osram/Lightify lights back to Osram hub~~
- ~~Add control panel/user interface (fix tablet/picture frame)~~
- ~~Turn on Flux when restarting server~~
- ~~Consider and setup sensor for mirror lights in upstairs bathroom~~
- ~~Fix arrival/Leaving work speaker notifications~~
- ~~Secondary switches (Osram/Hue) for when server is not working~~
- ~~When lights triggered manually make sure they stay on~~
- ~~Disable HVAC when window is open~~
- ~~Consider getting Sylvania lights on Hue~~ Would need to bug Smartthings support to install custom firmware or Osram, added Osram Hub which works just as well
- ~~Notification when there is a leak (high)~~

## Purchases
- (4) Add plug monitors/triggers (~~Laundry~~, Dryer, Coffee, Dishwasher)
- (4) Leak sensors (Laundry, ~~Crawl~~, Upstairs bathroom, Downstairs bathroom sink, downstairs bathroom doorway)
- ~~(2) Replace fireplace lights with Osram or Hue (get away from Zigbee connection to server) (this might be ok as is)~~
- (4) Motion Sensors (Master bedroom, hallway, downstairs bathroom, closet)
- (4) Outdoor/durable sensors (garage, sidewalk, patio, back yard)
- 48 port Gigabit switch (fan getting loud on the old one)
- Add house power monitor
- Roomba
- Door bell camera
- ~~(1-?) Back yard ambient lighting~~
- Outside speakers



